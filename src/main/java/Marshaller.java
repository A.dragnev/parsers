import java.io.File;

public interface Marshaller<T> {

    T read(File file);
    File write(T t);

}
