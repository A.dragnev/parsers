import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class JSON implements Marshaller<User>{
    @Override
    public User read(File file) {
        ObjectMapper mapper = new ObjectMapper();

        User user = null;
        try {
            user = mapper.readValue(file, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public File write(User user) {
        ObjectMapper mapper = new ObjectMapper();
        String path = "src/main/resources/";
        String name = "userJackson.json";
        File file = new File(path + name);
        try {
            mapper.writeValue(file, user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

}
