import javax.xml.bind.JAXB;
import java.io.File;

public class XML implements Marshaller<User>{

    @Override
    public User read(File file) {
        return JAXB.unmarshal(file, User.class);
    }

    @Override
    public File write(User user) {
        String path = "src/main/resources/";
        String name = "userJAXB.xml";
        File file = new File(path + name);
        JAXB.marshal(user, file);
        return file;
    }
}
