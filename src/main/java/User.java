public class User {

    private String name;
    private int age;
    private transient Object details;

    public User() {
       this.name = "Alex";
       this.age = 20;
        this.details = new Object();
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public Object getDetails() {
        return this.details;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "Marshaller.User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", details=" + details +
                '}';
    }
}
